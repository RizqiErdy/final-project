<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'postingan';
    protected $fillable = ['konten', 'gambar', 'user_id'];
}
