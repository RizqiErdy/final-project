@extends('layouts/master')

@section('judul')
    <i class="fa fa-stream mr-2"></i><b>My Timeline</b>
@endsection

@section('content')

<div class="row">
    <!-- Post Foto -->
    <div class="col-md-12">
      <!-- Box Postingan -->
      @foreach($post as $item)
      <div class="card card-widget">
        <!-- Nama User -->
        <div class="card-header">
          <div class="user-block">
            <img class="img-circle" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="User Image">
            <span class="username"><a href="#">Jonathan Burke Jr.</a></span>
            <span class="description">Shared publicly - 7:30 PM Today</span>
          </div>
        </div>
        <!-- /.End Nama User-->
        <!-- Isi Post -->
        <div class="card-body">
          <img class="img-fluid pad mb-2" src="{{asset('adminlte/dist/img/photo2.png')}}" alt="Photo">

        <p>{{$item->konten}}</p>
          <button type="button" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</button>
          <span class="float-right text-muted">127 likes</span>
        </div>
        <!-- /.End Isi Post -->
        <!-- Isi Komentar -->
        <div class="card-footer card-comments">
          <div class="card-comment">
            <!-- User image -->
            <img class="img-circle img-sm" src="{{asset('adminlte/dist/img/user3-128x128.jpg')}}" alt="User Image">

            <div class="comment-text">
              <span class="username">
                Maria Gonzales
                <span class="text-muted float-right">8:03 PM Today</span>
              </span><!-- /.username -->
              It is a long established fact that a reader will be distracted
              by the readable content of a page when looking at its layout.
            </div>
            <!-- /.comment-text -->
          </div>

          <!-- /.card-comment -->
          <div class="card-comment">
            <!-- User image -->
            <img class="img-circle img-sm" src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" alt="User Image">

            <div class="comment-text">
              <span class="username">
                Luna Stark
                <span class="text-muted float-right">8:03 PM Today</span>
              </span><!-- /.username -->
              It is a long established fact that a reader will be distracted
              by the readable content of a page when looking at its layout.
            </div>
            <!-- /.comment-text -->
          </div>
          <!-- /.card-comment -->
        </div>
        <!-- /.End Isi Komentar -->
        <!-- Form Komentar -->
        <div class="card-footer">
          <form action="#" method="post">
            <img class="img-fluid img-circle img-sm" src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" alt="Alt Text">
            <!-- .img-push is used to add margin to elements next to floating images -->
            <div class="img-push">
              <input type="text" class="form-control form-control-sm" placeholder="Press enter to post comment">
            </div>
          </form>
        </div>
        <!-- Form Komentar -->
      </div>
      @endforeach
      <!-- /.End Box Postingan -->
    </div>
    <!-- /.End Post Foto -->
    
    <!-- Post Quotes -->
    <div class="col-md-12">
      <!-- Box Postingan -->
      <div class="card card-widget">
        <!-- Nama User -->
        <div class="card-header">
          <div class="user-block">
            <img class="img-circle" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="User Image">
            <span class="username"><a href="#">Jonathan Burke Jr.</a></span>
            <span class="description">Shared publicly - 7:30 PM Today</span>
          </div>
        </div>
        <!-- /.End Nama User -->
        <div class="card-body">
          <!-- post text -->
          <p>Far far away, behind the word mountains, far from the
            countries Vokalia and Consonantia, there live the blind
            texts. Separated they live in Bookmarksgrove right at</p>

          <p>the coast of the Semantics, a large language ocean.
            A small river named Duden flows by their place and supplies
            it with the necessary regelialia. It is a paradisematic
            country, in which roasted parts of sentences fly into
            your mouth.</p>
          <!-- Social sharing buttons -->
          <button type="button" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</button>
          <span class="float-right text-muted">45 likes</span>
        </div>
        <!-- /.card-body -->
        <div class="card-footer card-comments">
          <div class="card-comment">
            <!-- User image -->
            <img class="img-circle img-sm" src="{{asset('adminlte/dist/img/user3-128x128.jpg')}}" alt="User Image">

            <div class="comment-text">
              <span class="username">
                Maria Gonzales
                <span class="text-muted float-right">8:03 PM Today</span>
              </span><!-- /.username -->
              It is a long established fact that a reader will be distracted
              by the readable content of a page when looking at its layout.
            </div>
            <!-- /.comment-text -->
          </div>
          <!-- /.card-comment -->
          <div class="card-comment">
            <!-- User image -->
            <img class="img-circle img-sm" src="{{asset('adminlte/dist/img/user5-128x128.jpg')}}" alt="User Image">

            <div class="comment-text">
              <span class="username">
                Nora Havisham
                <span class="text-muted float-right">8:03 PM Today</span>
              </span><!-- /.username -->
              The point of using Lorem Ipsum is that it hrs a morer-less
              normal distribution of letters, as opposed to using
              'Content here, content here', making it look like readable English.
            </div>
            <!-- /.comment-text -->
          </div>
          <!-- /.card-comment -->
        </div>
        <!-- /.card-footer -->
        <div class="card-footer">
          <form action="#" method="post">
            <img class="img-fluid img-circle img-sm" src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" alt="Alt Text">
            <!-- .img-push is used to add margin to elements next to floating images -->
            <div class="img-push">
              <input type="text" class="form-control form-control-sm" placeholder="Press enter to post comment">
            </div>
          </form>
        </div>
        <!-- /.card-footer -->
      </div>
      <!-- /.End Box Postingan -->
    </div>
    <!-- /.End Post Quotes -->
</div>
@endsection