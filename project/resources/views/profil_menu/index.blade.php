@extends('layouts/master')

@section('judul')
    <i class="fa fa-user mr-2"></i><b>My Profile</b>
@endsection

@section('content')

{{-- Pofile --}}
@if (session('Success'))
    <div class="alert alert-success">
        {{ session('Success') }}
    </div>
@endif
@if (session('Delete'))
    <div class="alert alert-danger">
    {{ session('Delete') }}
    </div>
@endif
<div class="col-md-12">
    <!-- Widget: user widget style 1 -->
    <div class="card card-widget widget-user shadow">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-info">
        <h3 class="widget-user-username">Alexander Pierce</h3>
        <h5 class="widget-user-desc">Founder & CEO</h5>
      </div>
      <div class="widget-user-image">
        <img class="img-circle elevation-2" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="User Avatar">
      </div>
      <div class="card-footer">
        <div class="row">
          <div class="col-sm-4 border-right">
            <div class="description-block">
              <h5 class="description-header">3,200</h5>
              <span class="description-text">Post</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-4 border-right">
            <div class="description-block">
              <h5 class="description-header">13,000</h5>
              <span class="description-text">FOLLOWING</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-4">
            <div class="description-block">
              <h5 class="description-header">35</h5>
              <span class="description-text">FOLLOWERS</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </div>
    <!-- /.widget-user -->
</div>
{{-- End Profile --}}

{{-- Postingan --}}
<div class="row">
    <!-- Post Foto -->
    <div class="col-md-12">
      <!-- Box Postingan -->
      <div class="card card-widget">
        <!-- Nama User -->
        <div class="card-header">
          <div class="user-block">
            <img class="img-circle" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="User Image">
            <span class="username"><a href="#">Jonathan Burke Jr.</a></span>
            <span class="description">Shared publicly - 7:30 PM Today</span>
          </div>
        </div>
        <!-- /.End Nama User-->
        <!-- Isi Post -->
        <div class="card-body">
          <img class="img-fluid pad mb-2" src="{{asset('adminlte/dist/img/photo2.png')}}" alt="Photo">

          <p>I took this photo this morning. What do you guys think?</p>
          {{-- Button --}}
          <span class="float-left">
            <button type="button" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</button>
            <a href="/post/show" class="btn btn-success btn-sm ml-1"> Show Post </a>
            <a href="/post/edit" class="btn btn-warning btn-sm ml-1"> Edit Post</a>
          </span>
          <span class="float-left">
            <form action="#" method="post">
                @csrf
                @method('DELETE')
                <input type="submit" value="Delete" onclick="return confirm('Ready to delete your post?')" class="btn btn-danger btn-sm ml-2">
            </form>
          </span>
          {{-- End Button --}}
          <span class="float-right text-muted">127 likes</span>
        </div>
        <!-- /.End Isi Post -->
        <!-- Isi Komentar -->
        <div class="card-footer card-comments">
          <div class="card-comment">
            <!-- User image -->
            <img class="img-circle img-sm" src="{{asset('adminlte/dist/img/user3-128x128.jpg')}}" alt="User Image">

            <div class="comment-text">
              <span class="username">
                Maria Gonzales
                <span class="text-muted float-right">8:03 PM Today</span>
              </span><!-- /.username -->
              It is a long established fact that a reader will be distracted
              by the readable content of a page when looking at its layout.
            </div>
            <!-- /.comment-text -->
          </div>
          <!-- /.card-comment -->
          <div class="card-comment">
            <!-- User image -->
            <img class="img-circle img-sm" src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" alt="User Image">

            <div class="comment-text">
              <span class="username">
                Luna Stark
                <span class="text-muted float-right">8:03 PM Today</span>
              </span><!-- /.username -->
              It is a long established fact that a reader will be distracted
              by the readable content of a page when looking at its layout.
            </div>
            <!-- /.comment-text -->
          </div>
          <!-- /.card-comment -->
        </div>
        <!-- /.End Isi Komentar -->
        <!-- Form Komentar -->
        <div class="card-footer">
          <form action="#" method="post">
            <img class="img-fluid img-circle img-sm" src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" alt="Alt Text">
            <!-- .img-push is used to add margin to elements next to floating images -->
            <div class="img-push">
              <input type="text" class="form-control form-control-sm" placeholder="Press enter to post comment">
            </div>
          </form>
        </div>
        <!-- Form Komentar -->
      </div>
      <!-- /.End Box Postingan -->
    </div>
    <!-- /.End Post Foto -->
    
    <!-- Post Quotes -->
    <div class="col-md-12">
      <!-- Box Postingan -->
      <div class="card card-widget">
        <!-- Nama User -->
        <div class="card-header">
          <div class="user-block">
            <img class="img-circle" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="User Image">
            <span class="username"><a href="#">Jonathan Burke Jr.</a></span>
            <span class="description">Shared publicly - 7:30 PM Today</span>
          </div>
        </div>
        <!-- /.End Nama User -->
        <div class="card-body">
          <!-- post text -->
          <p>Far far away, behind the word mountains, far from the
            countries Vokalia and Consonantia, there live the blind
            texts. Separated they live in Bookmarksgrove right at</p>

          <p>the coast of the Semantics, a large language ocean.
            A small river named Duden flows by their place and supplies
            it with the necessary regelialia. It is a paradisematic
            country, in which roasted parts of sentences fly into
            your mouth.</p>
          <!-- Social sharing buttons -->
          {{-- Button --}}
          <span class="float-left">
            <button type="button" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</button>
            <a href="/post/show" class="btn btn-success btn-sm ml-1"> Show Post </a>
            <a href="/post/edit" class="btn btn-warning btn-sm ml-1"> Edit Post</a>
          </span>
          <span class="float-left">
            <form action="#" method="post">
                @csrf
                @method('DELETE')
                <input type="submit" value="Delete" onclick="return confirm('Ready to delete your post?')" class="btn btn-danger btn-sm ml-2">
            </form>
          </span>
          {{-- End Button --}}
          <span class="float-right text-muted">45 likes</span>
        </div>
        <!-- /.card-body -->
        <div class="card-footer card-comments">
          <div class="card-comment">
            <!-- User image -->
            <img class="img-circle img-sm" src="{{asset('adminlte/dist/img/user3-128x128.jpg')}}" alt="User Image">

            <div class="comment-text">
              <span class="username">
                Maria Gonzales
                <span class="text-muted float-right">8:03 PM Today</span>
              </span><!-- /.username -->
              It is a long established fact that a reader will be distracted
              by the readable content of a page when looking at its layout.
            </div>
            <!-- /.comment-text -->
          </div>
          <!-- /.card-comment -->
          <div class="card-comment">
            <!-- User image -->
            <img class="img-circle img-sm" src="{{asset('adminlte/dist/img/user5-128x128.jpg')}}" alt="User Image">

            <div class="comment-text">
              <span class="username">
                Nora Havisham
                <span class="text-muted float-right">8:03 PM Today</span>
              </span><!-- /.username -->
              The point of using Lorem Ipsum is that it hrs a morer-less
              normal distribution of letters, as opposed to using
              'Content here, content here', making it look like readable English.
            </div>
            <!-- /.comment-text -->
          </div>
          <!-- /.card-comment -->
        </div>
        <!-- /.card-footer -->
        <div class="card-footer">
          <form action="#" method="post">
            <img class="img-fluid img-circle img-sm" src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" alt="Alt Text">
            <!-- .img-push is used to add margin to elements next to floating images -->
            <div class="img-push">
              <input type="text" class="form-control form-control-sm" placeholder="Press enter to post comment">
            </div>
          </form>
        </div>
        <!-- /.card-footer -->
      </div>
      <!-- /.End Box Postingan -->
    </div>
    <!-- /.End Post Quotes -->
</div>
{{-- End Postingan --}}

@endsection

