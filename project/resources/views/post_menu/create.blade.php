@extends('layouts/master')

@section('judul')
    <i class="fa fa-file-alt mr-2"></i><b>Create New Post</b>
@endsection

@section('content')
<a href="/post" class="btn btn-info btn-sm"><i class="fa fa-arrow-circle-left mr-1"></i> Back to My Profile </a>
<form action="/post" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="card-body">
        <div class="form-group">
            <label for="konten">New Post</label>
            <textarea name="konten" id="" class="form-control" placeholder="What's happening?"></textarea>
            @error('konten')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <input type='hidden' name='user_id' value='{{ Auth::user()->id }}' />
        <div class="form-group">
            <label for="gambar">Insert Image</label><br>
            <input type="file" name="gambar">
            @error('gambar')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Add New Post</button>
    </div>
    </form>   
@endsection
